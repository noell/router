# 简单的 react+react-router-v6 项目模板

1. 项目结构

```
.
├── READEME.md
├── index.html
├── package.json
├── src
│   ├── App.jsx
│   ├── main.jsx
│   ├── pages
│   │   ├── login
│   │   │   └── index.jsx
│   │   ├── main
│   │   │   ├── P1
│   │   │   │   └── index.jsx
│   │   │   ├── P2
│   │   │   │   └── index.jsx
│   │   │   └── index.jsx
│   │   └── welCome
│   │       └── index.jsx
│   └── router
│       └── index.jsx
├── vite.config.js
└── yarn.lock
```
