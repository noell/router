import React from "react";
import { useParams } from "react-router-dom";

export default function P1() {
  const { pages } = useParams();
  return <div>this is P1 这是第{pages}页</div>;
}
