import React, { Fragment } from "react";
import { Outlet } from "react-router-dom";

export default function Main() {
  return (
    <Fragment>
      <div className="Nav">Nav</div>
      <div className="body">
        <Outlet />
      </div>
    </Fragment>
  );
}
