import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "../pages/login";
import Main from "../pages/main";
import P1 from "../pages/main/P1";
import P2 from "../pages/main/P2";
import P1_1 from "../components/P1";
import WelCome from "../pages/welCome";
import Error from "../components/404Page";

export default function MyRouter() {
  return (
    <Router>
      <Routes>
        <Route index element={<WelCome />} />
        <Route path="/" element={<Main />}>
          //如果访问p1，默认跳转到第一页
          <Route path="/p1" element={<P1_1 />} />
          <Route path="/p1/:pages" element={<P1 />} />
          <Route path="/p2" element={<P2 />} />
        </Route>
        <Route path={"/login"} element={<Login />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </Router>
  );
}
