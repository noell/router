import React from "react";
import MyRouter from "./router";

export default function App() {
  return <MyRouter />;
}
