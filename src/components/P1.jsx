import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function P1() {
  const navigate = useNavigate();
  useEffect(() => {
    //如果访问/p1跳转到p1/1
    navigate("/p1/1");
  }, []);
  return <></>;
}
